package main

import (
	"fmt"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

// Lines of a template
const templateLines = 3

// Carousel implements tview's Grid for videos
type Carousel struct {
	*tview.Grid

	videoElements []*tview.TextView

	focus         int
	customHandler func(*tcell.EventKey) *tcell.EventKey
}

// New initiates a new carousel
func New() *Carousel {
	var grid = tview.NewGrid()

	c := &Carousel{
		Grid:          grid,
		videoElements: make([]*tview.TextView, 30),
	}

	grid.SetColumns(0)
	grid.SetBorders(false)
	grid.SetInputCapture(c.DefaultInputHandler)
	grid.SetGap(1, 1)

	var rows = make([]int, 30)
	for i := 0; i < 30; i++ {
		rows[i] = templateLines

		tv := tview.NewTextView()
		tv.SetText(fmt.Sprint("a multiline text\n", i, "widget\n"+"3rd line"))

		grid.AddItem(tv, i, 0, 1, templateLines, 0, 0, false)
		c.videoElements[i] = tv
	}

	grid.SetRows(rows...)

	return c
}

// DefaultInputHandler is the default input handler
func (c *Carousel) DefaultInputHandler(ev *tcell.EventKey) *tcell.EventKey {
	old := c.focus
	switch ev.Key() {
	case tcell.KeyUp:
		if c.focus == 0 {
			return ev
		}

		c.focus--

	case tcell.KeyDown:
		if c.focus == len(c.videoElements)-1 {
			return ev
		}

		c.focus++

	default:
		if c.customHandler != nil {
			return c.customHandler(ev)
		}

		return ev
	}

	c.videoElements[old].SetBackgroundColor(tcell.ColorBlack)
	c.videoElements[old].SetTextColor(tcell.ColorWhite)

	c.videoElements[c.focus].SetBackgroundColor(tcell.ColorWhite)
	c.videoElements[c.focus].SetTextColor(tcell.ColorBlack)

	c.SetOffset(c.focus-1, 0)

	return nil
}

// SetInputHandler sets a custom input capture
// Keys like Up/Down/Enter are swallowed
func (c *Carousel) SetInputHandler(f func(*tcell.EventKey) *tcell.EventKey) {
	c.customHandler = f
}
