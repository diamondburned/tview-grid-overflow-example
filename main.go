package main

import "github.com/rivo/tview"

func main() {
	tv := tview.NewTextView()
	tv.SetText(`watch
as
the
grid
takes
over
this
primitive
when you scroll with the arrow keys`)

	f := tview.NewFlex()
	f.SetDirection(tview.FlexRow)
	f.AddItem(tv, 0, 1, false)
	f.AddItem(New(), 0, 1, true)

	if err := tview.NewApplication().SetRoot(f, true).Run(); err != nil {
		panic(err)
	}
}
